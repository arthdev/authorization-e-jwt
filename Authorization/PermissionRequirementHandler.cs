using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Collections.Generic;

namespace AuthorizationJwt.Authorization
{
    public class PermissionRequirementHandler : AuthorizationHandler<PermissionRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            var userPermissions = GetUserPermissions(context);

            if (userPermissions.Any(p => p.Equals(requirement.Permission, StringComparison.Ordinal)))
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            context.Fail();
            return Task.CompletedTask;
        }

        private IEnumerable<string> GetUserPermissions(AuthorizationHandlerContext context)
        {
            var claim = context.User.FindFirst(ClaimTypes.Permission);

            if (claim != null)
            {
                return claim.Value.Split(',', StringSplitOptions.RemoveEmptyEntries);
            }
            
            return Enumerable.Empty<string>();
        }
    }
}