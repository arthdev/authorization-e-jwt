using System;
using System.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;

namespace AuthorizationJwt.Authorization
{
    public class JwtSettings
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public double Seconds { get; set; }
        public SigningCredentials SigningCredentials { get; }
        public JwtSettings()
        {
            // RFC indica 2048 para RSA
            // https://www.sharepointeurope.com/asp-net-core-how-to-digitally-sign-your-jwt/
            // https://www.brunobrito.net.br/jwt-assinaura-digital-rsa-ecdsa-hmac/

            // Gerar par de chaves aleratório
            SigningCredentials = new SigningCredentials(
                new RsaSecurityKey(RSA.Create(2048)), 
                SecurityAlgorithms.RsaSsaPssSha256);
        }
    }
}