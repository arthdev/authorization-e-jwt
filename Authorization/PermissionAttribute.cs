using Microsoft.AspNetCore.Authorization;

namespace AuthorizationJwt.Authorization
{
    public class PermissionAttribute : AuthorizeAttribute
    {
        public string Permission { get; }
        public PermissionAttribute(string requiredPermission)
        {
            Permission = requiredPermission;
            Policy = PolicyNames.Permission + Permission;
        }
    }
    
}