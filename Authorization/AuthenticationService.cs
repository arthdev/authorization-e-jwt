using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using AuthorizationJwt.Infra;
using Microsoft.IdentityModel.Tokens;

namespace AuthorizationJwt.Authorization
{
    public class AuthenticationService
    {
        private readonly JwtSettings _jwtSettings;
        private readonly BeerContext _dbContext;

        public AuthenticationService(JwtSettings jwtSettings, BeerContext dbContext)
        {
            _jwtSettings = jwtSettings;
            _dbContext = dbContext;
        }
        private User GetUser(string username)
        {
            return _dbContext.Set<User>().SingleOrDefault(u => u.Name.ToUpper().Equals(username.ToUpper()));
        }

        public string GenerateToken(string username, string password)
        {
            User user = GetUser(username);

            if (user == null || user.Password != password)
            {
                throw new Exception("Invalid password or username.");
            }

            DateTime iat = DateTime.UtcNow;
            DateTime exp = iat.AddSeconds(_jwtSettings.Seconds);

            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.CreateToken(new SecurityTokenDescriptor()
            {
                Issuer = _jwtSettings.Issuer,
                Audience = _jwtSettings.Audience,
                IssuedAt = iat,
                Expires = exp,
                NotBefore = iat,
                SigningCredentials = _jwtSettings.SigningCredentials,
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Sub, user.Name),
                    new Claim(JwtRegisteredClaimNames.Birthdate, user.BirthDate.ToString("O")),
                    new Claim(ClaimTypes.Permission, PermissionNames.ListBeers),
                })
            });

            return handler.WriteToken(jwtSecurityToken);
        }
    }
}