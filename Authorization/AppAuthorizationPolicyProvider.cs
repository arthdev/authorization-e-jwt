using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;

namespace AuthorizationJwt.Authorization
{
    public class AppAuthorizationPolicyProvider : IAuthorizationPolicyProvider
{
    private DefaultAuthorizationPolicyProvider _fallbackPolicyProvider;
    private readonly AuthorizationOptions _options;

    public AppAuthorizationPolicyProvider(IOptions<AuthorizationOptions> options)
    {
        _fallbackPolicyProvider = new DefaultAuthorizationPolicyProvider(options);
        _options = options.Value;
    }
    public Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
    {
        if (policyName.StartsWith(PolicyNames.Permission, StringComparison.OrdinalIgnoreCase))
        {
            var permission = policyName.Substring(PolicyNames.Permission.Length);
            var builder = CreatePolicyBuilder();
            builder.RequireAuthenticatedUser();
            builder.AddRequirements(new PermissionRequirement(permission));
            return Task.FromResult(builder.Build());
        }

        return _fallbackPolicyProvider.GetPolicyAsync(policyName);
    }

    public Task<AuthorizationPolicy> GetDefaultPolicyAsync()
    {
        var policy = CreatePolicyBuilder().RequireAuthenticatedUser().Build();
        return Task.FromResult(policy);
    }

    public Task<AuthorizationPolicy> GetFallbackPolicyAsync()
    {
        return Task.FromResult(_options.FallbackPolicy);
    }

    private AuthorizationPolicyBuilder CreatePolicyBuilder()
    {
        return new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme);
    }
}
}