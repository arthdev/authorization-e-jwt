using System;
using AuthorizationJwt.Authorization;
using AuthorizationJwt.Entities;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationJwt.Infra
{
    public class BeerContext : DbContext
    {
        public BeerContext(DbContextOptions<BeerContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>()
                        .HasKey(u => u.Id);
            modelBuilder.Entity<User>()
                        .Property(u => u.BirthDate)
                        .IsRequired();
            modelBuilder.Entity<User>()
                        .Property(u => u.Name)
                        .IsRequired();
            modelBuilder.Entity<User>()
                        .Property(u => u.Password)
                        .IsRequired();

            modelBuilder.Entity<Beer>()
                        .HasKey(b => b.Id);
            modelBuilder.Entity<Beer>()
                        .Property(b => b.Name)
                        .IsRequired();

            modelBuilder.Entity<User>()
                        .HasData(new User[] 
                                 {
                                     new User() 
                                     {
                                         Id = 1,
                                         Name = "Arthur",
                                         BirthDate = new DateTime(1995, 8, 27, 0, 0, 0, DateTimeKind.Utc),
                                         Password = "123123",
                                     }
                                 });
        }
    }
}