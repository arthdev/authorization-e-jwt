using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using AuthorizationJwt.Authorization;
using AuthorizationJwt.Infra;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationJwt
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddJsonOptions(options => {
                    // ASP .NET Core já configura por padrão:
                    // options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                    // options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                    // fonte: https://docs.microsoft.com/en-us/dotnet/standard/serialization/system-text-json-how-to?pivots=dotnet-core-3-1#web-defaults-for-jsonserializeroptions                    
                });
            
            var jwtSettings = new JwtSettings();
            new ConfigureFromConfigurationOptions<JwtSettings>(Configuration.GetSection("JwtSettings"))
                .Configure(jwtSettings);
            services.AddSingleton(jwtSettings);

            services.AddAuthentication(opt => {
               opt.DefaultAuthenticateScheme =  JwtBearerDefaults.AuthenticationScheme;
               opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = jwtSettings.SigningCredentials.Key,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = jwtSettings.Issuer,
                    ValidateIssuer = true,
                    ValidAudience = jwtSettings.Audience,
                    ValidateAudience = true,
                    ValidateLifetime = true
                };
            });

            services.AddDbContext<BeerContext>(o => o.UseInMemoryDatabase("beerdb"));

            services.AddSingleton<IAuthorizationPolicyProvider, AppAuthorizationPolicyProvider>();
            services.AddSingleton<IAuthorizationHandler, PermissionRequirementHandler>();
            services.AddScoped<AuthenticationService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHttpsRedirection();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            using (var scope = app.ApplicationServices.CreateScope())
            {
                scope.ServiceProvider.GetService<BeerContext>().Database.EnsureCreated();
            }
        }
    }
}
