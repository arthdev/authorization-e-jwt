namespace AuthorizationJwt.Dtos.Response
{
    public class AuthResult
    {
        public string Token { get; set; }

        public AuthResult(string token)
        {
            Token = token;
        }
    }
}