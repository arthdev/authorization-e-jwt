namespace JAuthorizationJwt.Dtos.Request
{
    public class GenerateTokenDto
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}