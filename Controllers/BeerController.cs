﻿using AuthorizationJwt.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationJwt.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BeerController : ControllerBase
    {
        private string[] _beers = new string[] 
        {
            "Coruja",
            "Badden Badden",
            "Saint Bier",
            "Lohn Bier",
            "Heineken",
            "Skol",
            "Glacial",
        };
        
        [HttpGet()]
        [Permission(PermissionNames.ListBeers)]
        public IActionResult Get()
        {
            return Ok(
                _beers[new System.Random().Next(0, _beers.Length - 1)]
            );
        }
    }
}
