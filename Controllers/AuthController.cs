using AuthorizationJwt.Dtos.Response;
using JAuthorizationJwt.Dtos.Request;
using AuthorizationJwt.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationJwt.Controllers
{
    [ApiController]
    [Route("auth")]
    public class AuthController : ControllerBase
    {
        private readonly AuthenticationService _service;

        public AuthController(AuthenticationService service)
        {
            _service = service;
        }

        [HttpPost("token")]
        public IActionResult Token([FromBody] GenerateTokenDto dto)
        {
            try
            {
                return Ok(
                    new AuthResult(_service.GenerateToken(dto.Username, dto.Password))
                );
            }
            catch (System.Exception)
            {
                return BadRequest();
            }
        }
    }
}